# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

1. Aprende a usar la herramienta Gulp para minificar y ofuscar código JavaScript
1. Modifica un fichero index.html para que la carga en web sea óptima usando todos los conocimientos adquiridos sobre optimización de JavaScript y CSS


# Instalación
-----------------------

```
$ vagrant up
```


# Instrucciones
-----------------------

### 1. Ejercicio Gulp

- Ejecuta `vagrant ssh` y sitúate dentro el directorio `src`
- Como puedes ver, hay un directorio `js/source` con varios ficheros JavaScript que deben ser minificados
- Uso los conocimientos adquiridos para generar un fichero con Gulp en `js/build/all.js` que esté minificado y ofuscado

### 2. Ejercicio Mejora rendimiento JavaScript/JS


- Entra en `http://1.2.3.4` con el navegador Google Chrome
- Usa las DeveloperTools para inspeccionar los requests que se hacen
- Abre el fichero con tu editor preferido `src/index.html` y aplica todas las mejoras aprendidas para que la web cargue mas rápido


# Desinstalación
-----------------------

```
$ vagrant destroy
```
